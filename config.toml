# Page settings

baseURL = "https://cv.stefan-lohr.com/"
languageCode = "en"
title = "Stefan Lohr"
theme = "hugo-devresume-theme"

[params]
    author = "Stefan Lohr"
    description = "Resume"

    primaryColor = "#54B689"
    textPrimaryColor = "#292929"

    [params.profile]
        enable = true
        name = "Stefan Lohr"
        tagline = "Cloud Security Expert"
        avatar = "avatar.jpg"

    [params.contact]
        enable = true
        location = "Munich"

        [[params.contact.list]]
        icon = "fas fa-envelope-square"
        url = "mailto:mail@stefan-lohr.com"
        text = "mail@stefan-lohr.com"
        [[params.contact.list]]
        icon = "fas fa-globe"
        url = "https://stefan-lohr.com"
        text = "www.stefan-lohr.com"

    [params.skills]
        enable = true

        [[params.skills.list]]
        title = "AWS"
        [[params.skills.list.items]]
        details = "Architect, Security Architect, Developer"
        
        [[params.skills.list]]
        title = "Azure, GCP"
        [[params.skills.list.items]]
        details = "Security Architect, Developer"

        [[params.skills.list]]
        title = "DevSecOps"
        [[params.skills.list.items]]
        details = "Git, Gitlab, Gitlab CI"
        [[params.skills.list.items]]
        details = "Terraform, Ansible, Packer"
        [[params.skills.list.items]]
        details = "Container, k8s, k3s"

        [[params.skills.list]]
        title = "Agile"
        [[params.skills.list.items]]
        details = "Scrum, Kanban"
        
        [[params.skills.list]]
        title = "Professional"
        [[params.skills.list.items]]
        details = "Effective communication"
        [[params.skills.list.items]]
        details = "Team player"
        [[params.skills.list.items]]
        details = "Strong problem solver"
        [[params.skills.list.items]]
        details = "Good strategic thinking"
        [[params.skills.list.items]]
        details = "Good time management"
        

    [params.education]
        enable = false

        [[params.education.list]]
        degree = "MSc in Computer Science"
        university = "University College London"
        dates = "2010 - 2011"
        [[params.education.list]]
        degree = "BSc Maths and Physics"
        university = "Imperial College London"
        dates = "2007 - 2010"

    [params.awards]
        enable = false

        [[params.awards.list]]
        name = "Award Lorem Ipsum"
        body = "Microsoft lorem ipsum"
        date = "2019"
        [[params.awards.list]]
        name = "Award Donec Sodales"
        body = "Oracle Aenean"
        date = "2017"

    [params.languages]
        enable = true

        [[params.languages.list]]
        name = "Bavarian"
        level = "Native"
        [[params.languages.list]]
        name = "German"
        level = "Fluent"
        [[params.languages.list]]
        name = "English"
        level = "Professional"

    [params.interests]
        enable = true

        [[params.interests.list]]
        name = "Roleplaying"
        [[params.interests.list]]
        name = "Homelab"
        [[params.interests.list]]
        name = "Homeautomation"
        [[params.interests.list]]
        name = "Hiking"
        [[params.interests.list]]
        name = "Skiing"

    [params.summary]
        enable = true
        text = "I am a professional with an entrepreneurial spirit. My excellent expertise and broad professional experience enable me to make well-considered and realistic decisions, even in complex and difficult situations."


    [params.experience]
        enable = true

        [[params.experience.list]]
        title = "Cybersecurity Cloud Topic Owner / Senior Key Expert Cloud Services"
        dates = "2019 - present"
        company = "Siemens"
        details = "I work on strategic topics for Siemens AG in close collaboration with all the business units and internal audit. Identifying trends and prioritize them for Cybersecurity in alignment with IT Governance. I act as single point of contact for cloud security for our management, cybersecurity community as well as business management."
        [[params.experience.list.items]]
        details = "Lead Architect: Security Northstar"
        [[params.experience.list.items]]
        details = "Stakeholder management, Customer relations"
        [[params.experience.list.items]]
        details = "zero trust, CASB"
        [[params.experience.list.items]]
        details = "aws, azure, gcp"

        [[params.experience.list]]
        title = "Cloud Security Architect"
        dates = "2016 - 2019"
        company = "Siemens"
        details = "In 2014 Siemens started their cloud journey with the CloudFirst@Siemens program, in which I was part of the security project. Part of my achievements was the design of a general network architecture and role model as well as strengthen the DevSecOps teams. After the project ended I still was in very high demand when it came to consulting for big as well as small - but critical - projects in their cloud journey. I organized and held multiple trainings for the cybersecurity community and devops teams."
        [[params.experience.list.items]]
        details = "aws"
        [[params.experience.list.items]]
        details = "git, gitlab ci, inner source"
        [[params.experience.list.items]]
        details = "terraform, cloud formation, ansible"
        [[params.experience.list.items]]
        details = "trainings: AWS architecture; application design for cloud environments; automate security; proper authentication;"

        [[params.experience.list]]
        title = "Software Architect / Lead Developer"
        dates = "2015 - 2019"
        company = "Siemens"
        details = "Also as a Software Architect I joined the Cybersecurity department and was responsible for the cybersecurity dashboard __Caremore__. Caremore is a service that collects, processes and combines security-relevant information about the important assests of Siemens and makes it availble to the Cybersecurity community in a transparent way. Moreover I was able to convince my management and the datacenter management that we should use a fully automated ci/cd pipeline."
        [[params.experience.list.items]]
        details = "C#, HTML, JS"
        [[params.experience.list.items]]
        details = "MS SQL Server, TSQL"
        [[params.experience.list.items]]
        details = "git, ci/cd, gitlab ci"
        [[params.experience.list.items]]
        details = "docker, openshift"
        [[params.experience.list.items]]
        details = "oauth2, oidc"
        [[params.experience.list.items]]
        details = "micro services"
        [[params.experience.list.items]]
        details = "Scrum"

        [[params.experience.list]]
        title = "Software Architect"
        dates = "2012 - 2015"
        company = "Siemens"
        details = "In the role of an Software Architect and Application Manager I was responsible for a Siemens internal audit application called __Multi-Audit-Control Board__. In addition to this role I was also nominated as the Security Lead for my department."
        [[params.experience.list.items]]
        details = "Java, Groovy"
        [[params.experience.list.items]]
        details = "Oracle, PL/SQL, Apex"
        [[params.experience.list.items]]
        details = "OWASP, penetration testing for web applications"
        [[params.experience.list.items]]
        details = "Kanban"

        [[params.experience.list]]
        title = "Software Architect / Senior Software Developer"
        dates = "2005 - 2012"
        company = "Dräxlmaier Group"
        details = "The Dräxlmaier Group was in need for a central transport and custom management software and I was given the chance to design and develop a new solution from scrath. this solution was integrated with the order management and the shopfloor management system. "
        [[params.experience.list.items]]
        details = "Java, C#, Delphi, SVN, HTML, JS"
        [[params.experience.list.items]]
        details = "Oracle, PL/SQL"
        [[params.experience.list.items]]
        details = "Technical team lead, Topic Lead for Databases, Delphi"

        [[params.experience.list]]
        title = "Software Developer"
        dates = "2001 - 2005"
        company = "Dräxlmaier Group"
        details = "As a developer I was responsible for the internal order management system. "
        [[params.experience.list.items]]
        details = "Delphi"
        [[params.experience.list.items]]
        details = "Oracle, PL/SQL"
                

    [params.projects]
        enable = true

        [[params.projects.list]]
        title = "Cloud Security Monitoring"
        meta = "Product Owner / 2020-2021"
        tagline = "The goal of this solution was to combine the cloud provider native security services in one place and integrate this in already existing solutions like reporting, asset management, exception handling as well as vulnerability management."

        [[params.projects.list]]
        title = "Cloud Security Project"
        meta = "Technical Director / 2018 - present"
        tagline = "The goal was to close all identified gaps in the Siemens infrastructure when it comes to cloud computing and establish services that can and will maintain the deliverables."

        [[params.projects.list]]
        title = "CloudFirst@Siemens"
        meta = "Core Member / 2015 - 2018"
        tagline = "As part of the security workstream it was our job to make sure that the transition to the cloud was secure, but still viable for the DevOps teams. My main contributions: implement DevSecOps mindset, network architectures, IAM role model."

        [[params.projects.list]]
        title = "Long-term Backup"
        meta = "Project Manager / 2010-2011"
        tagline = "Long-term Backup in central datacenter in near real time with fast read performance to the data."


    [params.information]
        enable = true

        [[params.information.list]]
        title = "AWS Enterprise User Group"
        details = "The AWS enterprise user group consists of various german based companies from whom I am the elected leader. Together in this group we address and change a lot of common issues from german companies with the support of AWS. One example would be the IAM Boundaries pushed by this group."
        
        [[params.information.list]]
        title = "CSSA - Siemens Representative"
        details = "I represent Siemens in the _Cyber Security Sharing & Analytics Group_ in all workstreams regarding Cloud."

        [[params.information.list]]
        title = "Papers"
        [[params.information.list.items]]
        details = "2018 · _Co-Author_ · __Siemens Cloud Whitepaper__"
        [[params.information.list.items]]
        details = "2016 · _Core Team Member_ · __Siemens Cloud Security Standard__"

    [params.social]
        enable = true

        [[params.social.list]]
        icon = "fab fa-github"
        url = "//github.com/stefanlohr"
        title = "stefanlohr"
        [[params.social.list]]
        icon = "fab fa-gitlab"
        url = "//gitlab.com/slohr"
        title = "slohr"
        [[params.social.list]]
        icon = "fab fa-linkedin"
        url = "//linkedin.com/in/stefan-lohr-2799681b1/"
        title = "stefan-lohr"
        [[params.social.list]]
        icon = "fab fa-twitter"
        url = "//twitter.com/IwamaRyu"
        title = "@IwamaRyu"
